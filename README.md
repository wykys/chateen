# Chateen [![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

![Made with Python](https://img.shields.io/badge/Made%20with-Python-4584b6.svg)
![Made with Jinja](https://img.shields.io/badge/Made%20with-Jinja-b80000.svg)
![Made with Bootstrap](https://img.shields.io/badge/Made%20with-Bootstrap-7952b3.svg)

Chateen is a tool for preparing data for language analysis.

Chateen uses its own implementation of a simple RAM database, which speeds up data processing, as there is no need to translate database requests into SQL calls.

## Supported formats

Supported formats for data import:
* Facebook `JSON`
* Instagram `HTML`

Supported formats for data export:
* Language corpus `<s>Message content</s>`
* Website `HTML`

## Install the required packages

```
pip3 install -r requirements.txt
```
