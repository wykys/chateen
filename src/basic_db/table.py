from .register import Register
from .column import Column


class Table(object):

    def __init__(self):
        super().__init__()
        # inicializace datových typů sloupců instancí tabulek
        for base in self.__class__.__bases__ + (self.__class__, ):
            if issubclass(base, Table):
                for key, value in base.__dict__.items():
                    if isinstance(value, Column):
                        setattr(self, key, value.type())

    def get(self, **kwargs):
        return Register.get(self, kwargs)

    def add(self):
        Register.add(self)

    def __str__(self):
        return str(self.__dict__)
