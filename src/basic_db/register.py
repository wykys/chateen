from .list import List
from .column import Column
from copy import deepcopy


class Register(object):
    def __call__(self):
        return self

    def __init__(self):
        super().__init__()
        self._register = dict()

    def __repr__(self):
        txt = ''
        for table in self._register:
            txt += table + ':\n'
            for item in self._register[table]:
                txt += '\t' + str(item) + '\n'
        return txt

    def new_table(self, cls):
        """
        Přidá novou tabulku do databáze.
        """
        from .table import Table

        if not issubclass(cls, Table):
            print('Error: The registered table must be based on the Table class.')
            exit(-1)

        self._register[cls.__name__] = List()

        # vytvoření hlubokých kopií vnořených atributů do hlavní třídy
        for base in cls.__bases__:
            if issubclass(base, Table):
                for key, value in base.__dict__.items():
                    if isinstance(value, Column):
                        setattr(cls, key, deepcopy(value))

        # inicializace sloupců tabulky pro vyhledávání
        for key, value in cls.__dict__.items():
            if isinstance(value, Column):
                value.attr = key
                value.cls = cls

        return cls

    def add(self, cls):
        """
        Přidání nové instance do tabulky.
        """
        cls.id = len(self._register[cls.__class__.__name__])
        self._register[cls.__class__.__name__].append(cls)

    def get(self, cls, kwargs):
        """
        Vrátí seznam instancí tabulky.
        """
        res = self._register[cls.__class__.__name__]
        for key, value in kwargs.items():
            res_old = res
            res = List()
            for r in res_old:
                if getattr(r, key) == value:
                    res.append(r)
        return res


Register = Register()
