# zobecněný list, který uchovává záznamy
# snaží se být kompatibilní s python list
# kvůli intuitivnímu používání

from types import GeneratorType


class List(object):

    def __init__(self, data=None):
        if data is None:
            self._data = list()
        elif isinstance(data, GeneratorType):
            self._data = list(data)
        else:
            self._data = data

    def __repr__(self) -> str:
        return self._data.__repr__()

    def __len__(self):
        return len(self._data)

    def __contains__(self, key):
        return key in self._data

    def __getitem__(self, key):
        return self._data[key]

    def __setitem__(self, key, value):
        self._data[key] = value

    def __iter__(self):
        self._iter_index = 0
        self._iter_stop = self.__len__()
        return self

    def __next__(self):
        if self._iter_index == self._iter_stop:
            raise StopIteration
        tmp = self._data[self._iter_index]
        self._iter_index += 1
        return tmp

    def first(self):
        return self._data[0]

    def last(self):
        return self._data[-1]

    def count(self):
        return len(self._data)

    def append(self, object):
        self._data.append(object)

    def filter(self, select):
        return List(
            item for item in select if item in self._data
        )

    def sort(self, key, reverse=False):
        def evaluation_function(item):
            from .column import Column

            if isinstance(key, Column):
                attr = getattr(item, key.attr)
            else:
                attr = getattr(item, key)

            if callable(attr):
                return attr()
            else:
                return attr

        return List(
            sorted(self._data, key=evaluation_function, reverse=reverse)
        )
