# Sloupec tabulky

from .list import List


class Column(object):
    def __init__(self, type):
        self.cls = None
        self.attr = None
        self.type = type

    def check_param(self, param):
        if isinstance(param, List):
            return param.count()
        else:
            return param

    def __lt__(self, param):
        """
        self < param
        """
        return List(item for item in self.cls().get()
                    if self.check_param(getattr(item, self.attr)) < param)

    def __le__(self, param):
        """
        self <= param
        """
        return List(item for item in self.cls().get()
                    if self.check_param(getattr(item, self.attr)) <= param)

    def __gt__(self, param):
        """
        self > param
        """
        return List(item for item in self.cls().get()
                    if self.check_param(getattr(item, self.attr)) > param)

    def __ge__(self, param):
        """
        self >= param
        """
        return List(item for item in self.cls().get()
                    if self.check_param(getattr(item, self.attr)) >= param)

    def __eq__(self, param):
        """
        self == param
        """
        return List(item for item in self.cls().get()
                    if self.check_param(getattr(item, self.attr)) == param)

    def __ne__(self, param):
        """
        self != param
        """
        return List(item for item in self.cls().get()
                    if self.check_param(getattr(item, self.attr)) != param)
