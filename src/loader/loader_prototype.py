# Prototyp objektu pro načítání dat do databáze.
# wykys 2020

from pathlib import Path
from types import FunctionType

UNKNOWN_AUTHOR = 'Fejkař Otto'


class LoaderPrototype(object):
    def __init__(
        self,
        path: str,
        chat_name: str = None,
        callback_progress: FunctionType = None
    ) -> None:

        super().__init__()
        self.chat_name = chat_name
        self.callback_progress = callback_progress
        self.percent = None
        if self.set_path(path):
            self.load()
            self.decode()

    def set_path(self, path: str) -> bool:
        p = Path(path)
        if p.is_file():
            self.path = path
            self.file = path.split('/')[-1]
            return True

        print(f'File {path} is not exist!')
        return False

    def load(self) -> None:
        print('Load is not defined!')

    def decode(self) -> None:
        print('Decode is not defined!')

    def progress(self, percent: int, file: str = '') -> None:
        percent = int(percent)
        if self.percent != percent:
            self.percent = percent
            if self.callback_progress is not None:
                self.callback_progress.emit(percent)
            else:
                print(f'\rImport progress: {percent} % ... {file}', end='')
                if percent == 100:
                    print()
