#!/usr/bin/env python3
# Modul pro načtení Instagram HTML do databáze.
# wykys 2021

from bs4 import BeautifulSoup
from types import FunctionType
from datetime import datetime, timedelta
from models import Chat, Message, Participant
from .loader_prototype import LoaderPrototype, UNKNOWN_AUTHOR


class IgLoader(LoaderPrototype):
    def __init__(
        self,
        path: str,
        chat_name: str = None,
        callback_progress: FunctionType = None
    ):
        super().__init__(path, chat_name, callback_progress)

    def load(self):
        with open(self.path, 'r', encoding='utf-8') as fr:
            self.html_doc = fr.read()

    def decode(self):

        soup = BeautifulSoup(self.html_doc, 'html.parser')

        chat = Chat()
        chat.name = self.path.split('/')[-1].split('.html')[0].strip()
        chat.add()

        self.progress(0)

        top_level_div = soup.find('div', {'class': '__VUU41'})

        top_level = top_level_div.find_all('div', recursive=False)
        number_of_tags = len(top_level)
        tags_counter = 0

        for packing_div in top_level:

            packing_class = packing_div.get('class')
            tags_counter += 1
            self.progress(100 * tags_counter / number_of_tags, self.file)

            # pokud div nemá přířazené třídy, tak ho přeskočit
            if not isinstance(packing_class, list):
                continue

            # přeskočení bloků, neobsahujících hledané informace
            if '__Igw0E' not in packing_class:
                continue

            flag_time = False
            flag_author = False
            flag_message = False

            # extrakce data
            if '__IwRSH' in packing_class:
                div = packing_div.find(
                    'div', {'class': '__FBi-h'}, recursive=False)
                moon, day, year = map(int, div.text.split('/'))
                base_date = datetime(year, moon, day)

            # extrakce zprávy
            elif '__Xf6Yq' in packing_class:
                for div in packing_div.find_all('div', recursive=False):
                    cls = div.get('class')

                    # pokud div nemá přířazené třídy, tak ho přeskočit
                    if not isinstance(cls, list):
                        continue

                    # div obsahující informace o času a autora
                    if '__JI_ht' in cls:
                        # čas
                        time_div = div.find(
                            'div', {'class': '__fDxYl'}, recursive=False)
                        if time_div is not None:
                            time_span = time_div.find(
                                'span', recursive=False)
                            if time_span is not None:
                                time_str, am_pm = time_span.text.split()
                                am_pm = 0 if am_pm.upper() == 'AM' else 12
                                h, m, s = map(int, time_str.split(':'))
                                time_delta = timedelta(
                                    hours=h + am_pm,
                                    minutes=m,
                                    seconds=s
                                )
                                flag_time = True

                        # autor správy
                        author_div = div.find(
                            'div', {'class': '__hLiUi'})
                        if author_div is not None:
                            author_img = author_div.find('img')
                            if author_img is not None:
                                alt = author_img.get('alt')
                                if 'profile picture' in alt:
                                    author = alt.split('profile picture')[
                                        0].strip() or UNKNOWN_AUTHOR
                                    flag_author = True

                        # prázdné správy
                        else:
                            # TODO: prozkoumat na více datech.
                            break

                    # div s obsahem zprávy
                    elif '__vwCYk' in cls:
                        message_span = div.find('span')
                        message_audio = div.find('audio')
                        if message_span is not None:
                            message = message_span.text
                            flag_message = True
                        elif message_audio is not None:
                            message = 'Audio: ' + message_audio.get('src')
                            flag_message = True

                if flag_author and flag_time and flag_message:

                    participant = Participant().get(name=author)

                    if participant.count() == 0:
                        participant = Participant()
                        participant.name = author
                        participant.add()
                    else:
                        participant = participant.first()

                    if participant not in chat.participants:
                        participant.chats.append(chat)
                        chat.participants.append(participant)

                    msg = Message()
                    msg.participant = participant
                    msg.text = message
                    msg.datetime = base_date + time_delta
                    chat.messages.append(msg)
                    participant.messages.append(msg)
                    msg.add()
