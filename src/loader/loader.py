from types import FunctionType
from .loader_json_fb import FbLoader
from .loader_html_ig import IgLoader


class Loader(object):
    def __init__(
        self,
        path: str,
        chat_name: str = None,
        callback_progress: FunctionType = None
    ) -> None:
        super().__init__()

        path = str(path)

        if path.endswith(('.json', '.JSON', )):
            FbLoader(path, chat_name, callback_progress)
        elif path.endswith(('.html', '.HTML', )):
            IgLoader(path, chat_name, callback_progress)
        else:
            print('Unsupported file extension.')
