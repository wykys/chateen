# Modul pro načtení Facebook JSON do databáze.
# wykys 2021

import json
from datetime import datetime
from types import FunctionType
from models import Chat, Message, Participant
from .loader_prototype import LoaderPrototype, UNKNOWN_AUTHOR


SUPPRESS_TEXT = 'Anketa už není dostupná.'


class FbLoader(LoaderPrototype):
    def __init__(
        self,
        path: str,
        chat_name: str = None,
        callback_progress: FunctionType = None
    ):
        super().__init__(path, chat_name, callback_progress)

    def load(self):
        def fix_fb_code(obj):
            def fix(s: str) -> str:
                return s.encode('latin_1').decode('utf-8')

            for key in obj:
                if isinstance(obj[key], str):
                    obj[key] = fix(obj[key])
                elif isinstance(obj[key], list):
                    obj[key] = list(
                        map(
                            lambda x:
                                fix(x) if isinstance(x, str) else x,
                                obj[key]
                        )
                    )
            return obj

        with open(self.path, 'r', encoding='utf-8') as fr:
            self.data = json.load(fr, object_hook=fix_fb_code)

    def decode(self):
        self.progress(0)

        if self.chat_name is None:
            chat = Chat()
            chat.name = self.path.split('/')[-1].split('.json')[0].strip()
            chat.add()
        else:
            chat = Chat().get(name=self.chat_name)
            if chat.count() == 0:
                chat = Chat()
                chat.name = self.chat_name
                chat.add()
            else:
                chat = chat.first()

        tags_counter = 0
        number_of_tags = len(self.data['messages'])

        for fb_msg in self.data['messages']:

            tags_counter += 1
            self.progress(100 * tags_counter /
                          number_of_tags, self.file)

            if not ('content' in fb_msg or 'audio_files' in fb_msg):
                continue
            elif not ('sender_name' in fb_msg and 'timestamp_ms' in fb_msg):
                continue
            elif 'type' not in fb_msg or fb_msg['type'] != 'Generic':
                continue

            if 'content' in fb_msg and fb_msg['content'] == SUPPRESS_TEXT:
                continue

            author = fb_msg['sender_name'] or UNKNOWN_AUTHOR
            participant = Participant().get(name=author)

            if participant.count() == 0:
                participant = Participant()
                participant.name = author
                participant.add()
            else:
                participant = participant.first()

            if participant not in chat.participants:
                participant.chats.append(chat)
                chat.participants.append(participant)

            msg = Message()
            msg.participant = participant

            if 'content' in fb_msg:
                msg.text = fb_msg['content']

            elif 'audio_files' in fb_msg:
                audio = fb_msg['audio_files'][0]['uri']
                msg.text = f'Audio: {audio}'

            msg.datetime = datetime.fromtimestamp(
                int(fb_msg['timestamp_ms']) / 1000)

            chat.messages.append(msg)
            participant.messages.append(msg)
            msg.add()
