from .loader import Loader
from .loader_json_fb import FbLoader
from .loader_html_ig import IgLoader


__all__ = [
    'Loader', 'FbLoader', 'IgLoader'
]
