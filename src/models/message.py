from basic_db import Register, Column
from datetime import datetime

from .base import BaseModel
from .chat import Chat
from .participant import Participant


@Register.new_table
class Message(BaseModel):

    text = Column(str)
    datetime = Column(datetime.now)
    chat = Column(Chat)
    participant = Column(Participant)

    def __repr__(self):
        return f'Message: {self.text}'

    def __str__(self):
        return f'{self.datetime.strftime("%Y/%m/%d %H:%M:%S")} {self.participant.name:30}: {self.text}'
