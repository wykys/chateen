from basic_db import Table, Column


class BaseModel(Table):

    id = Column(int)
