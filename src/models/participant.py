from basic_db import Register, List, Column
from .base import BaseModel


@Register.new_table
class Participant(BaseModel):

    name = Column(str)
    messages = Column(List)
    chats = Column(List)

    def __repr__(self):
        return f'{self.name}'

    def __str__(self):
        return f'{self.name}:\n *  chats: {self.chats.count()}\n *  messages: {self.messages.count()}'
