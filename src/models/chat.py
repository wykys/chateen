from basic_db import Register, List, Column
from .base import BaseModel


@Register.new_table
class Chat(BaseModel):

    name = Column(str)
    messages = Column(List)
    participants = Column(List)

    def __repr__(self):
        return f'Chat: {self.name}'
