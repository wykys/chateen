#!/usr/bin/env python3
# wykys 2020
#               ,,
#   .g8"""bgd `7MM                 mm
# .dP'     `M   MM                 MM
# dM'       `   MMpMMMb.   ,6"Yb.mmMMmm .gP"Ya   .gP"Ya `7MMpMMMb.
# MM            MM    MM  8)   MM  MM  ,M'   Yb ,M'   Yb  MM    MM
# MM.           MM    MM   ,pm9MM  MM  8M"""""" 8M""""""  MM    MM
# `Mb.     ,'   MM    MM  8M   MM  MM  YM.    , YM.    ,  MM    MM
#   `"bmmmd'  .JMML  JMML.`Moo9^Yo.`Mbmo`Mbmmd'  `Mbmmd'.JMML  JMML.
#

from datetime import date, datetime
from loader import Loader
from argparse import ArgumentParser
from exporter import (ExporterCorpusChat,
                      ExporterCorpusParticipant, ExporterHtmlChat)

from pathlib import Path

if __name__ == '__main__':

    parser = ArgumentParser(
        'chateen',
        description='Chateen is a tool for preparing data for language analysis.',
    )
    parser.add_argument(
        '-i',
        '--import',
        dest='input',
        default='../data',
        action='store',
        help='folder or file to import',
    )
    parser.add_argument(
        '-e',
        '--export',
        dest='output',
        default='../export',
        action='store',
        help='folder with files to export',
    )
    parser.add_argument(
        '-t',
        '--import-format',
        dest='import_format',
        choices=[
            'fb', 'ig', 'all',
        ],
        default='all',
        help='format of imported data'
    )
    parser.add_argument(
        '-b',
        '--bulk-import',
        dest='bulk_import',
        action='store_true',
        default=False,
        help='imports subfolders as separate chats'
    )
    parser.add_argument(
        '-f',
        '--export-format',
        dest='export_format',
        choices=[
            'corpus', 'html', 'all',
        ],
        default='all',
        help='format of exported data'
    )
    parser.add_argument(
        '--from',
        dest='date_from',
        type=date.fromisoformat,
        default=None,
        help='from which date the messages are to be exported (iso format 2012-06-21)'
    )
    parser.add_argument(
        '--to',
        dest='date_to',
        type=date.fromisoformat,
        default=None,
        help='to which data the messages are to be exported (iso format 1980-07-31)'
    )

    args = parser.parse_args()

    import_path = Path(args.input)
    export_path = args.output
    import_format = args.import_format
    export_format = args.export_format
    bulk_import = args.bulk_import
    date_from = args.date_from
    if date_from is not None:
        date_from = datetime(date_from.year, date_from.month, date_from.day)
    date_to = args.date_to
    if date_to is not None:
        date_to = datetime(date_to.year, date_to.month, date_to.day)

    if import_path.is_dir():

        top_chat_name = str(import_path).split('/')[-1]

        for top_path in import_path.iterdir():

            if bulk_import and top_path.is_dir():
                sub_chat_name = str(top_path).split('/')[-1]
                for sub_path in top_path.iterdir():
                    if sub_path.is_file():
                        Loader(sub_path, sub_chat_name)

            elif top_path.is_file():
                Loader(top_path, top_chat_name)

    elif import_path.is_file():
        Loader(import_path)

    else:
        print('Wrong input directory.')
        exit(-1)

    if export_format == 'corpus' or export_format == 'all':
        ExporterCorpusChat(
            f'{export_path}/corpus_chat',
            date_from,
            date_to
        )
        ExporterCorpusParticipant(
            f'{export_path}/corpus_nick',
            date_from,
            date_to
        )

    if export_format == 'html' or export_format == 'all':
        ExporterHtmlChat(
            f'{export_path}/html_chat',
            date_from,date_to
        )
