from types import FunctionType
from datetime import date
from models import Participant, Message
from .exporter_prototype import ExporterPrototype


class ExporterCorpusParticipant(ExporterPrototype):
    def __init__(
        self,
        path: str,
        date_from: date = None,
        date_to: date = None,
        callback_progress: FunctionType = None
    ) -> None:

        super().__init__(path, date_from, date_to, callback_progress)

    def export(self) -> None:
        self.progress(0)

        for participant in Participant().get():

            tags_counter = 0

            participant_messages = list([
                msg
                for msg in participant.messages.sort(key=Message.datetime)
                if self.is_msg_datetime_in_interval(msg)
            ])
            number_of_tags = len(participant_messages)

            if number_of_tags == 0:
                continue

            file = f'{participant.name}.txt'.replace('/', '_')
            with open(f'{self.path}/{file}', 'w') as fw:
                for msg in participant_messages:
                    fw.write(
                        f'<s>{msg.text}</s>\n'
                    )

                    tags_counter += 1
                    self.progress(100 * tags_counter / number_of_tags, file)
