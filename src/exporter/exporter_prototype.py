# Prototyp objektu pro načítání dat do databáze.
# wykys 2020

from datetime import datetime
from pathlib import Path
from types import FunctionType
from models.message import Message

class ExporterPrototype(object):
    def __init__(
        self,
        path: str,
        date_from: datetime = None,
        date_to: datetime = None,
        callback_progress: FunctionType = None
    ) -> None:

        super().__init__()
        self.date_from = date_from
        self.date_to = date_to
        self.callback_progress = callback_progress
        self.percent = None
        if self.set_path(path):
            self.export()

    def set_path(self, path: str) -> bool:
        p = Path(path)
        p.mkdir(parents=True, exist_ok=True)
        if p.is_dir():
            self.path = path
            return True
        print(f'Can not open: {path} dir!')
        return False

    def export(self) -> None:
        print('Export is not defined!')

    def is_msg_datetime_in_interval(self, msg: Message) -> bool:
        if self.date_from is not None and self.date_to is not None:
            return (
                msg.datetime >= self.date_from and msg.datetime <= self.date_to
            )
        elif self.date_from is not None:
            return (
                msg.datetime >= self.date_from
            )
        elif self.date_to is not None:
            return (
                msg.datetime <= self.date_to
            )
        else:
            return True

    def progress(self, percent: int, file: str = '') -> None:
        percent = int(percent)
        if self.percent != percent:
            self.percent = percent
            if self.callback_progress is not None:
                self.callback_progress.emit(percent)
            else:
                print(
                    f'\rExport progress: {percent:3} % ... {file:42}', end='')
                if percent == 100:
                    print()
