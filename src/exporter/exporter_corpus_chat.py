from types import FunctionType
from datetime import datetime
from models import Chat, Message
from .exporter_prototype import ExporterPrototype


class ExporterCorpusChat(ExporterPrototype):
    def __init__(
        self,
        path: str,
        date_from: datetime = None,
        date_to: datetime = None,
        callback_progress: FunctionType = None
    ) -> None:

        super().__init__(path, date_from, date_to, callback_progress)

    def export(self) -> None:
        self.progress(0)

        for chat in Chat().get():

            tags_counter = 0

            chat_messages = list([
                msg
                for msg in chat.messages.sort(key=Message.datetime)
                if self.is_msg_datetime_in_interval(msg)
            ])
            number_of_tags = len(chat_messages)

            if number_of_tags == 0:
                continue

            file = f'{chat.name}.txt'.replace('/', '_')
            with open(f'{self.path}/{file}', 'w') as fw:
                for msg in chat_messages:
                    fw.write(
                        f'<s><nick: {msg.participant.name}>{msg.text}</s>\n'
                    )

                    tags_counter += 1
                    self.progress(100 * tags_counter / number_of_tags, file)
