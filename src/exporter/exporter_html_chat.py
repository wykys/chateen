from datetime import datetime
from htmlmin import minify
from types import FunctionType
from jinja2 import Environment, FileSystemLoader
from models import Chat, Message
from .exporter_prototype import ExporterPrototype


class ExporterHtmlChat(ExporterPrototype):
    def __init__(
        self,
        path: str,
        date_from: datetime = None,
        date_to: datetime = None,
        callback_progress: FunctionType = None
    ) -> None:

        super().__init__(path, date_from, date_to, callback_progress)

    def export(self) -> None:
        self.progress(0)

        for chat in Chat().get():

            file = f'{chat.name}.html'.replace('/', '_')

            file_loader = FileSystemLoader('./exporter')
            env = Environment(loader=file_loader)
            template = env.get_template('base.html')

            chat_messages = list([
                msg
                for msg in chat.messages.sort(key=Message.datetime)
                if self.is_msg_datetime_in_interval(msg)
            ])
            number_of_tags = len(chat_messages)

            if number_of_tags == 0:
                continue

            html_doc = template.render(
                title=chat.name,
                messages=chat_messages,
                number_of_tags=number_of_tags,
                file=file,
                progress=self.progress,
            )

            with open(f'{self.path}/{file}', 'w') as fw:
                fw.write(minify(html_doc, remove_empty_space=True))
